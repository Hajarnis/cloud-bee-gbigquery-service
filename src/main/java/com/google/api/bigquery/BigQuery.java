package com.google.api.bigquery;


 
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.lang.reflect.Field;
import java.util.List;

import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.services.bigquery.Bigquery;
import com.google.api.services.bigquery.BigqueryScopes;
import com.google.api.services.bigquery.model.GetQueryResultsResponse;
import com.google.api.services.bigquery.model.QueryRequest;
import com.google.api.services.bigquery.model.QueryResponse;
import com.google.api.services.bigquery.model.TableCell;
import com.google.api.services.bigquery.model.TableRow;

/**
 * Example of authorizing with BigQuery and reading from a public wikipedia dataset.
 *
 */
public class BigQuery {
  
  /**
   * using Application Default Credentials for Authentication client service .
   * 
   */
  public static Bigquery createAuthorizedClient() throws IOException {
    // Create the credential
    HttpTransport transport = new NetHttpTransport();
    JsonFactory jsonFactory = new JacksonFactory();
    GoogleCredential credential = GoogleCredential.getApplicationDefault(transport, jsonFactory);

    if (credential.createScopedRequired()) {
      credential = credential.createScoped(BigqueryScopes.all());
    }

    return new Bigquery.Builder(transport, jsonFactory, credential)
        .setApplicationName("Bigquery Samples")
        .build();
  }
  
  /**
   * Executes the given query synchronously.
   */
  private static List<TableRow> executeQuery(String querySql, Bigquery bigquery, String projectId)
      throws IOException {
    QueryResponse query =
        bigquery.jobs().query(projectId, new QueryRequest().setQuery(querySql)).execute();

    // Execute it
    GetQueryResultsResponse queryResult =
        bigquery
            .jobs()
            .getQueryResults(
                query.getJobReference().getProjectId(), query.getJobReference().getJobId())
            .execute();

    
    return queryResult.getRows();
  }

  /**
   * Display result to console
   */
  private static void displayResults(List<TableRow> rows) {
	  
    System.out.print("\nResults:\n------------\n");
    for (TableRow row : rows) {
      for (TableCell field : row.getF()) {
        System.out.printf("%-50s", field.getV());
      }
      System.out.println();
    }
  }

  /**
   * Exercises the methods defined in this class.
   *
   * In particular, it creates an authorized Bigquery service object using Application Default
   * Credentials, then executes a query against the public Shakespeare dataset and prints out the
   * results.
   *
   * @param args the first argument, if it exists, should be the id of the project to run the test
   *     under. If no arguments are given, it will prompt for it.
   * @throws IOException if there's an error communicating with the API.
   */
  public static void main(String[] args) throws IOException {
    
    String projectId = "lively-epsilon-170708";
    File file = new File("./Output/Output.txt");
	FileWriter fw;

	long startTime = System.currentTimeMillis();
	long endTime;
	
	System.out.println("Invoking the Google BigQuery at :- " + System.currentTimeMillis());
	
    // Create a new Bigquery client authorized via Application Default Credentials.
    Bigquery bigquery = createAuthorizedClient();

    List<TableRow> rows =
        executeQuery(
            "SELECT																			"	+
"  REGEXP_EXTRACT(line, r'\"([^\"]+)\"') AS url,											"	+
"  COUNT(*) AS count																		"	+
" FROM																						"	+
"  FLATTEN( (																				"	+
"    SELECT																					"	+
"      SPLIT(SPLIT(REGEXP_EXTRACT(content, r'.*import\\s*[(]([^)]*)[)]'), '\n'), ';') AS line,"	+
"    FROM (																					"	+
"      SELECT																				"	+
"        id,																				"	+
"        content																			"	+
"      FROM																					"	+
"        [bigquery-public-data:github_repos.sample_contents]								"	+
"      WHERE																				"	+
"        REGEXP_MATCH(content, r'.*import\\s*[(][^)]*[)]')) AS C								"	+
"    JOIN (																					"	+
"      SELECT																				"	+
"        id																					"	+
"      FROM																					"	+
"        [bigquery-public-data:github_repos.sample_files]									"	+
"      WHERE																					"	+
"        path LIKE '%.go'																	"	+
"      GROUP BY																				"	+
"        id) AS F																			"	+
"    ON																						"	+
"      C.id = F.id), line)																	"	+
"GROUP BY																					"	+
"  url																						"	+
"HAVING																						"	+
"  url IS NOT NULL																			"	+
"ORDER BY																					"	+
"  count DESC																				"	+
"LIMIT 10",
            bigquery,
            projectId);

    displayResults(rows);
    
	try {
		fw = new FileWriter(file);
		BufferedWriter bw = new BufferedWriter(fw);
		fw.write(rows.toString());
		fw.close();
	} catch (IOException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}

    endTime = System.currentTimeMillis();
    
    System.out.println("Finished. Execution time " + (System.currentTimeMillis() - startTime)/(100*60) + " secs.");
  }
}
// [END all]
